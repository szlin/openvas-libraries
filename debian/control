Source: gvm-libs
Section: libs
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: SZ Lin (林上智) <szlin@debian.org>
Build-Depends: debhelper-compat (= 12),
               cmake,
               libglib2.0-dev,
               libgpgme11-dev,
               libgnutls28-dev,
               libldap2-dev,
               libssh-gcrypt-dev,
               libhiredis-dev,
               libradcli-dev,
               pkg-config,
               uuid-dev
Build-Depends-Indep: doxygen
Homepage: https://www.greenbone.net/
Vcs-Browser: https://salsa.debian.org/pkg-security-team/gvm-libs
Vcs-Git: https://salsa.debian.org/pkg-security-team/gvm-libs.git
Standards-Version: 4.4.1
Rules-Requires-Root: no

Package: libgvm-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libgcrypt-dev,
         libglib2.0-dev,
         libgnutls28-dev,
         libgpgme-dev,
         libhiredis-dev,
         libksba-dev,
         libgvm11 (= ${binary:Version}),
         libpcap-dev,
         libssh-dev,
         uuid-dev,
         libsnmp-dev
Suggests: libgvm-doc
Description: remote network security auditor - static libraries and headers
 The Open Vulnerability Assessment System is a modular security auditing
 tool, used for testing remote systems for vulnerabilities that should be
 fixed.
 .
 It is made up of two parts: a server, and a client. The server/daemon,
 openvasd, is in charge of the attacks, whereas the client,
 OpenVAS-Client, provides an X11/GTK+ user interface.
 .
 This package contains the required static libraries, headers, and
 libopenvas-config script.

Package: libgvm-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: remote network security auditor - libraries documentation
 The Open Vulnerability Assessment System is a modular security auditing
 tool, used for testing remote systems for vulnerabilities that should be
 fixed.
 .
 It is made up of two parts: a server, and a client. The server/daemon,
 openvasd, is in charge of the attacks, whereas the client,
 OpenVAS-Client, provides an X11/GTK+ user interface.
 .
 This package contains the doxygen generated HTML documentation for the
 libraries.


Package: libgvm11
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Multi-Arch: same
Replaces: libopenvas9
Conflicts: libopenvas9
Breaks: openvas-scanner (<< 5.1.2~)
Description: remote network security auditor - shared libraries
 The Open Vulnerability Assessment System is a modular security auditing
 tool, used for testing remote systems for vulnerabilities that should be
 fixed.
 .
 It is made up of two parts: a server, and a client. The server/daemon,
 openvasd, is in charge of the attacks, whereas the client,
 OpenVAS-Client, provides an X11/GTK+ user interface.
 .
 This package contains the required shared libraries.
